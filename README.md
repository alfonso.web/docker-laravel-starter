<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

-   [Simple, fast routing engine](https://laravel.com/docs/routing).
-   [Powerful dependency injection container](https://laravel.com/docs/container).
-   Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
-   Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
-   Database agnostic [schema migrations](https://laravel.com/docs/migrations).
-   [Robust background job processing](https://laravel.com/docs/queues).
-   [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

-   **[Vehikl](https://vehikl.com/)**
-   **[Tighten Co.](https://tighten.co)**
-   **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
-   **[64 Robots](https://64robots.com)**
-   **[Cubet Techno Labs](https://cubettech.com)**
-   **[Cyber-Duck](https://cyber-duck.co.uk)**
-   **[Many](https://www.many.co.uk)**
-   **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
-   **[DevSquad](https://devsquad.com)**
-   [UserInsights](https://userinsights.com)
-   [Fragrantica](https://www.fragrantica.com)
-   [SOFTonSOFA](https://softonsofa.com/)
-   [User10](https://user10.com)
-   [Soumettre.fr](https://soumettre.fr/)
-   [CodeBrisk](https://codebrisk.com)
-   [1Forge](https://1forge.com)
-   [TECPRESSO](https://tecpresso.co.jp/)
-   [Runtime Converter](http://runtimeconverter.com/)
-   [WebL'Agence](https://weblagence.com/)
-   [Invoice Ninja](https://www.invoiceninja.com)
-   [iMi digital](https://www.imi-digital.de/)
-   [Earthlink](https://www.earthlink.ro/)
-   [Steadfast Collective](https://steadfastcollective.com/)
-   [We Are The Robots Inc.](https://watr.mx/)
-   [Understand.io](https://www.understand.io/)
-   [Abdel Elrafa](https://abdelelrafa.com)
-   [Hyper Host](https://hyper.host)
-   [Appoly](https://www.appoly.co.uk)
-   [OP.GG](https://op.gg)
-   [云软科技](http://www.yunruan.ltd/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Laravel Starter - Docker for PHP Developers

A Laravel starter project for Docker using Nginx and PHP-FPM. This project is an opinionated approach to organizing Docker in a PHP project and demonstrates separating configuration and code for containers.

## Setup

To start using this project, there are a few setup steps that you need to take which include things like composer installation and setting up env files.

Here's the commands you need to run to get going:

```
# From the root of the project

# Copy the example env files
cp .env.example .env
cp .docker.env.example .docker.env

# Install composer dependencies
composer install

# Generate a key
php artisan key:generate

# Install frontend code
yarn && yarn dev

# Or if you use NPM
npm install && npm run dev

# Build the Docker images and run containers in the background
# Omit the `-d` flag if you want to run in the foreground
docker-compose up --build -d

# If you run in the background, you can see the logs with:
docker-compose logs -f

# Migrate the database in the container
docker-compose run --rm app /bin/bash

# Now inside the container
php artisan migrate
```

## Connecting to the Database

When you have the Docker environment running you can connect to a database via a GUI application or MySQL command line. Here's how you would connect from the console, for local development:

```
mysql -u root -proot -P13306 -h 127.0.0.1
```

The `docker-compose.yml` file contains the environment configuration for the MySQL database name, users, and root password for reference. The `mysql` service in the Docker Compose file maps port `13306` on the host machine to `3306` on the container so that it doesn't interrupt any local MySQL instances you might have running. I've just added a `1` in front of the `3306` as `13306` if you want an easy way to remember the port exposed for MySQL.

Here's what a Sequel Pro connection might look like:

![Sequel Pro Connection Details](resources/doc/images/sequel-pro-connection.png)

## Code Organization

The source code is a Laravel 5.5 project with the Docker configuration needed to run application containers, schedulers, and queues. Here is an overview of the Docker file organization. All of the Docker setup files are located in the `docker/` folder, with the exception of the `docker-compose.yml` and `.docker.env.example` file.

Here is a quick overview of the Docker setup files:

```
docker/
├── Dockerfile
├── confd
│   ├── conf.d
│   │   └── nginx-default.toml
│   └── templates
│       └── nginx-default.tmpl
├── nginx
│   └── h5bp
├── php
│   ├── composer-installer.sh
│   ├── conf.d
│   │   ├── opcache.ini
│   │   └── xdebug.ini
│   ├── php-fpm.d
│   │   └── docker.conf
│   └── php.ini
├── run-app.sh
└── supervisor
    ├── conf.d
    │   ├── app.conf
    │   ├── queue.conf
    │   └── scheduler.conf
    └── supervisord.conf
```

### The Dockerfile

The `Dockerfile` is used to build the PHP image that runs the Laravel application.

### Confd

The `confd` folder contains configuration files and templates for [confd](https://github.com/kelseyhightower/confd), which is used to separate configuration and code in containers. This starter project contains one example of this in action: you can use environment to change the Nginx `server_name` directive.

For example, when the container starts, the `docker/run-app.sh` file runs `confd -onetime -backend env` which then creates the `/etc/nginx/sites-available/default` file, using the `NGINX_SERVER_NAME` environment variable to set the `server_name`. If you were to set `NGINX_SERVER_NAME=example.dev`, the output would be:

```
server {
    # This template tag
    server_name {{getenv "NGINX_SERVER_NAME"}};

    # Will become...
    server_name example.dev
}
```

### Nginx

The `nginx/` folder contains HTML 5 Boilerplate configuration for Nginx servers (basic configuration).

The basic configuration includes the following, which are pretty self-evidient:

```
# Basic h5bp rules

include h5bp/directive-only/x-ua-compatible.conf;
include h5bp/location/expires.conf;
include h5bp/location/cross-domain-fonts.conf;
include h5bp/location/protect-system-files.conf;
```

### PHP

The `php` folder contains PHP INI configuration files for Xdebug and Confd. You can add your own `.ini` files to the `docker/php/conf.d/` folder, and PHP will read them when you build the Docker image.

The `docker/php/conf.d/xdebug.ini` file demonstrates how you can use environment variables to control Xdebug, which allows each developer the ability to modify these settings however he or she sees fit.

The `docker/php/conf.d/opcache.ini` file is an important configuration file to check out. If you set the environment variable `PHP_OPCACHE_VALIDATE_TIMESTAMPS=0`, your file modifications will not be recognized. This is the defaut and recommended setting for production. In development, as you will see in the `.docker.env.example` file, the value `PHP_OPCACHE_VALIDATE_TIMESTAMPS=1` ensures that each new change will be recognized in development. You should read through each setting carefully and adapt to your project needs.

The `docker/php/php-fpm.d/docker.conf` file configures PHP-FPM to listen on a Unix socket, which is then configured in Nginx.

### run-app.sh

The `docker/run-app.sh` file is copied into the container and is configured as the following in the `docker/Dockerfile`:

```
CMD ["/usr/local/bin/run-app"]
```

The `run-app.sh` file runs `supervisord` to run the webserver, scheduler, and queue, but before executing supervisord, the script will prepare the runtime environment.

First, if the `APP_ENV` is not `local`, then a few `artisan` commands are executed, specifically `php artisan config:cache` and `php artisan route:cache`. Non-development environments shouldn't have Xdebug configured as a module, so the `run-app.sh` script removes all Xdebug INI files when `APP_ENV` is not `local`.

Next, if you are having permission issues on Linux systems, you can set the `DEV_UID` environment variable to allow Docker to modify files like cache, etc. If you are on OS X, you should not have to configure `DEV_UID`. You can get your local user UID by running `echo $UID` from the command line, and setting the value in the `.docker.env` file.

You can configure the `CONTAINER_ROLE` environment variable, which changes how the container will run. This allows one image to run as an application server, or a scheduler, or a queue. This means that you don't have three separate builds. Based on container role, the `run-app.sh` file will symlink the appropriate supervisor configuration based on the type of container you want to run.

### Supervisor

The `supervisor/` folder contains three configuration files in `docker/supervisor/conf.d`, and a main `supervisord.conf` file.

The `docker/supervisor/conf.d` configuration files include `app.conf`, `queue.conf`, and `scheduler.conf`. Based on the `CONTAINER_ROLE` environment variable, the `run-app.sh` script will symlink the right conf file in place at runtime.

When running as a web server, `supervisord` runs both Nginx and PHP-FPM, which communicate via a Unix socket.

You might find the `docker/supervisor/conf.d/scheduler.conf` file the most interesting:

```
[program:laravel-scheduler]
user=www-data
command=bash -c "while true; do php /var/www/html/artisan schedule:run --verbose --no-interaction & sleep 60; done"
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
```

The `command` property is an infinite bash loop that runs the `artisan schedule:run` command in the background, and then sleep for `60` seconds before running the `artisan schedule:run` command again. The bash loop keeps the process running in the foreground, and acts like a cron to run the scheduler in a Docker container.

## License

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
